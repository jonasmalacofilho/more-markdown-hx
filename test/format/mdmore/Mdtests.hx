package format.mdmore;

import utest.Assert;

@:autoBuild(format.mdmore.MdtestsBuilder.build())
private class Mdtests {

    public
    function new(html:String->String) {
        this.html = html;
    }

    dynamic
    function html(markdown:String):String {
        throw null;
    }

}

@:path("tests/mdtest_basic")
class BasicMdtests extends Mdtests {
}

@:path("tests/mdtest_php")
class PhpMdtests extends Mdtests {
}

@:path("tests/mdtest_php_extra")
class PhpExtraMdtests extends Mdtests {
}

