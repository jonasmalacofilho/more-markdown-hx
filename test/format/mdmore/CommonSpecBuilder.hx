package format.mdmore;

import haxe.macro.Expr;
import haxe.macro.Context;

class CommonSpecBuilder {

    public
    macro static function build(path:String):Array<Field> {
#if display
        return null;
#end
        var fields = Context.getBuildFields();

        if (!sys.FileSystem.exists(path)) {
            var cls = Context.getLocalClass().get();
            var loc = Context.resolvePath(cls.module.split(".").join("/") + ".hx").split("/");
            var pathPreffix = loc.slice(0, loc.length - 1).join("/") + "/";
            path = pathPreffix + path;
        }
        var spec = sys.io.File.getContent(path);

        var test = ~/^\.\n([\s\S]*?)^\.\n([\s\S]*?)^\.$/gm;
        var testNo = 0;
        var testPos = 0;
        var testLen = 0;
        while (test.match(spec)) {
            testNo++;
            testPos += testLen + test.matchedPos().pos;
            testLen = test.matchedPos().len;
            
            fields.push(buildTest(testNo, test.matched(1), test.matched(2)));

            spec = test.matchedRight();
        }
        
        return fields;
    }
    
    static function buildTest(testNo:Int, markdown:String, html:String):Field {
        var field = {
            name : "test" + StringTools.lpad(Std.string(testNo), "0", 4),
            doc : null,
            meta : [],
            access : [APublic],
            kind : FFun({
                args : [],
                ret : null,
                expr : macro {
                    var expected = Tools.tidy($v{html});
                    var result = Tools.tidy(html($v{markdown}));
                    Assert.equals(expected, result, Tools.errorMsg(expected, result));
                }
            }),
            pos : Context.currentPos()
        };
        return field;
    }

}

