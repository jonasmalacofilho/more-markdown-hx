package format.mdmore;

import haxe.macro.Expr;
import haxe.macro.Context;

class MdtestsBuilder {

    public
    macro static function build():Array<Field> {
#if display
        return null;
#end
        var fields = [];
        var path:String;

        var cls = Context.getLocalClass().get();
        var loc = Context.resolvePath(cls.module.split(".").join("/") + ".hx").split("/");
        var preffix = loc.slice(0, loc.length - 1).join("/") + "/";
        
        for (meta in cls.meta.get()) {
            switch (meta) {
                case { name : metaName, params : [{ expr : EConst(CString(testPath)) }] }:
                    if (metaName == ":path" || metaName == "path") {
                        path = testPath;
                    }
                case _: // noop
            }
        }

        var testNames = sys.FileSystem.readDirectory(preffix + path)
                .filter(function (name) return ~/.text$/.match(name))
                .map(function (name) return name.substr(0, name.length - 5));
        for (testName in testNames) {
            fields.push(buildTest(cls, preffix + path + "/", testName));
        }
        return fields;
    }
    
    static function buildTest(cls, preffix:String, name:String):Field {
        var markdown = sys.io.File.getContent(preffix + name + ".text");
        var html;
        try {
            html = sys.io.File.getContent(preffix + name + ".xhtml");
        }
        catch (e:String) {
            html = sys.io.File.getContent(preffix + name + ".html");
        }
        var testName = "test" + ~/[ (),-]+/g.replace(name, "_");
        var field = {
            name : testName,
            doc : null,
            meta : [],
            access : [APublic],
            kind : FFun({
                args : [],
                ret : null,
                expr : macro {
                    var expected = Tools.tidy($v{html});
                    var result = Tools.tidy(html($v{markdown}));
                    Assert.equals(expected, result, Tools.errorMsg(expected, result));
                }
            }),
            pos : Context.currentPos()
        };
        return field;
    }

}

