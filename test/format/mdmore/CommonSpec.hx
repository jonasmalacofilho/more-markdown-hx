package format.mdmore;

import utest.Assert;

@:build(format.mdmore.CommonSpecBuilder.build("tests/common/spec.txt"))
class CommonSpec {

    public
    function new(html:String->String) {
        this.html = html;
    }

    dynamic
    function html(markdown:String):String {
        throw null;
    }

}

