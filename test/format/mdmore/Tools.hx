package format.mdmore;

class Tools {
    
    public
    static function errorMsg(expected:String, result:String):String {
        // compare and stop at the first difference
        var minLen = expected.length;
        if (result.length < expected.length) minLen = result.length;
        var pos = 0;
        while (pos < minLen && expected.charAt(pos) == result.charAt(pos)) pos++;
        var before = 10, after = before, width = before + 1 + after;
        if (pos - before < 0) pos = before;

        // slice both `expected` and `result` around that point
        expected = expected.substr(pos - before, width);
        result = result.substr(pos - before, width);

        return "\n\texpected " + q(expected) +
               "\n\tbut was  " + q(result);
    }

    static function q(v) {
        return '"' + StringTools.replace(v, '"', '\\"') + '"';
    }

    public
    static function tidy(html:String):String {
        var out = [];
        var inPre = false;
        var transfs = [
            // partially based on @jgm/stmd/runtests.pl
            function (inp) return ~/^$/g.replace(inp, ""), // skip blank lines
            function (inp) return ~/\n/g.replace(inp, " "),  // transform line breaks into spaces
            function (inp) return ~/^ +/g.replace(inp, ""),  // remove leading spaces
            function (inp) return ~/ +$/g.replace(inp, ""),  // remove trailing spaces
            function (inp) return ~/  +/g.replace(inp, " "),  // colapse consecutive spaces
            function (inp) return ~/^ +\/>/g.replace(inp, "/>"),  // collapse space before /> in tag
            function (inp) return ~/> </g.replace(inp, "><"),  // remove spaces between tags
        ];
        function getMarkup(start) {
            var pre = ~/<pre/;
            if (pre.matchSub(html, start)) {
                inPre = true;
                start = pre.matchedPos().pos;
                var end = ~/>/;
                if (!end.matchSub(html, start)) throw "Incomplete <pre";
                var endPos = end.matchedPos();
                return start + endPos.pos + endPos.len;
            }
            return -1;
        }
        function getRaw(start) {
            inPre = false;
            var end = ~/<\/pre>/;
            if (end.matchSub(html, start)) {
                var endPos = end.matchedPos();
                return endPos.pos + endPos.len;
            }
            return -1;
        }
        function transform(text) {
            if (!inPre) {
                for (t in transfs) {
                    text = t(text);
                }
            }
            return text;
        }
        var pos = 0;
        while (pos < html.length) {
            var end = inPre ? getRaw(pos) : getMarkup(pos);
            if (end == -1) end = html.length;
            out.push(transform(html.substring(pos, end)));
            pos = end;
        }
        return out.join("");
    }

}

