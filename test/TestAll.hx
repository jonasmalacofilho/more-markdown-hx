import format.mdmore.CommonSpec;
import format.mdmore.Mdtests;
import utest.ui.common.HeaderDisplayMode;

class TestAll {

    static function main() {
        var testRunner = new utest.Runner();

        function html(markdown:String):String {
#if markdown
            return Markdown.markdownToHtml(markdown);
#else
            return markdown;
#end
        }

        // @jgm/stmd (CommonMark)
        testRunner.addCase(new CommonSpec(html));

        // michelf/mdtests
        testRunner.addCase(new BasicMdtests(html));
        testRunner.addCase(new PhpMdtests(html));
        testRunner.addCase(new PhpExtraMdtests(html));
        
        utest.ui.Report.create(testRunner);

        testRunner.run();
    }

}

